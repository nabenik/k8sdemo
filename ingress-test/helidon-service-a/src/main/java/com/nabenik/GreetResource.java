
package com.nabenik;

import java.util.Collections;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/hello")
public class GreetResource {
   
    @Inject
    @ConfigProperty(defaultValue = "A Service", name = "extmessage")
    String message;
    
    @GET
    public String getDefaultMessage() {
        return "Message from service a " + message;
    }
}
