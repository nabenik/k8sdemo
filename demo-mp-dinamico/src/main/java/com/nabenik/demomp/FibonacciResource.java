package com.nabenik.demomp;

import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/fibo")
public class FibonacciResource {

    @GET
    public int getFibo(@QueryParam("it") Integer iterations){
        return this.fibonacci(iterations);
    }

    public static int fibonacci(int number){
        if(number == 1 || number == 2){
            return 1;
        }
      
        return fibonacci(number-1) + fibonacci(number -2); //tail recursion
    }
    
}
