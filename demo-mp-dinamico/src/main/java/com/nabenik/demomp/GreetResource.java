
package com.nabenik.demomp;

import java.util.Collections;
import java.util.Optional;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * A simple JAX-RS resource to greet you. Examples:
 *
 * Get default greeting message:
 * curl -X GET http://localhost:8080/greet
 *
 * The message is returned as a JSON object.
 */
@Path("/greet")
public class GreetResource {
    private static final JsonBuilderFactory JSON = Json.createBuilderFactory(Collections.emptyMap());

    @Inject
    @ConfigProperty(defaultValue = "Hello", name = "greeting")
    Optional<String> saludo;

    @GET
    public String doHello(){
        if(saludo.isPresent()) {
            return saludo.get() + " mundo";
        }else {
            return "No se encontro la variable";
        }
    }
}
