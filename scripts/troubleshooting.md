# Troubleshooting 

## Arquitectura de logs

* stout and stderr
* Logs fisicos - /var/log/containers
* Agregación de logs (systemd y journalctl)
* kubectl logs - Funcionamiento de PODs y deployments

## Toubleshooting POD

* kubectl logs <podname> -n kube-system -c weave-npc -f
* kubectl logs --selector app=nginx-sc-deploy --all-containers
* kubectl describe pod sc-deploy-7d999968bd-5xr5l

## Troubleshooting host

* kubelet
    * systemd service
    * journald 
    * journalctl -f kubelet.service
    * /var/log/kubelet.log
    * journalctl al sistema, dmesg, tail -f /var/log/message
* kube-proxy
    * Pod
    * kubectl logs
    * /var/log/containers
    * /var/log/kube-proxy
    * journalctl al sistema, dmesg, tail -f /var/log/message

## Eventos

Ver eventos

* kubectl get events -w
* kubectl get events --field-selector type=Warning  -w