/etc/ssl/
* openssl genrsa -out gitlabci.key 2048
* openssl genrsa -out jquinonez.key 2048
* openssl genrsa -out victor.key 2048


* openssl req -new -key gitlabci.key -out gitlabci.csr -subj "/CN=gitlabci"
* openssl req -new -key jquinonez.key -out jquinonez.csr -subj "/CN=jquinonez"
* openssl req -new -key victor.key -out victor.csr -subj "/CN=victor"


* cat gitlabci.csr | base64 | tr -d "\n" > gitlabci.base64.csr
* cat jquinonez.csr | base64 | tr -d "\n" > jquinonez.base64.csr
* cat victor.csr | base64 | tr -d "\n" > victor.base64.csr

* cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: gitlabci
spec:
  groups:
  - system:authenticated  
  request: $(cat gitlabci.base64.csr)
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
EOF
* cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: jquinonez
spec:
  groups:
  - system:authenticated  
  request: $(cat jquinonez.base64.csr)
  signerName: kubernetes.io/kube-apiserver-client
  usages:
  - client auth
EOF

* kubectl certificate approve gitlabci
* kubectl certificate approve jquinonez

* kubectl get certificatesigningrequests gitlabci \
  -o jsonpath='{ .status.certificate }'  | base64 --decode > gitlabci.crt
* kubectl get certificatesigningrequests jquinonez \
  -o jsonpath='{ .status.certificate }'  | base64 --decode > jquinonez.crt


* openssl x509 -in gitlabci.crt -text -noout | head -n 15
* openssl x509 -in jquinonez.crt -text -noout | head -n 15

* kubectl create clusterrolebinding gitlabciadminclusterrolebinding \
  --clusterrole=cluster-admin --user=gitlabci

*  kubectl create clusterrolebinding gitlabciclusterrolebinding \
  --clusterrole=admin --user=gitlabci
* kubectl create clusterrolebinding jquinonezclusterrolebinding \
  --clusterrole=admin --user=jquinonez

*  kubectl create rolebinding gitlabcisicomprolebinding \
  --clusterrole=admin --user=gitlabci --namespace=sicomp
*  kubectl create rolebinding gitlabcisiamprolebinding \
  --clusterrole=admin --user=gitlabci --namespace=siamp
* kubectl create rolebinding gitlabcidefaultrolebinding \
  --clusterrole=admin --user=gitlabci --namespace=default

* kubectl config set-cluster kubernetes \
  --server=https://vip-k8s-master:6443 \
  --certificate-authority=/etc/kubernetes/pki/ca.crt \
  --embed-certs=true \
  --kubeconfig=gitlabci.conf
* kubectl config set-cluster kubernetes \
  --server=https://vip-k8s-master:6443 \
  --certificate-authority=/etc/kubernetes/pki/ca.crt \
  --embed-certs=true \
  --kubeconfig=jquinonez.conf

* kubectl config set-credentials gitlabci \
  --client-key=gitlabci.key \
  --client-certificate=gitlabci.crt \
  --embed-certs=true \
  --kubeconfig=gitlabci.conf
* kubectl config set-credentials jquinonez \
  --client-key=jquinonez.key \
  --client-certificate=jquinonez.crt \
  --embed-certs=true \
  --kubeconfig=jquinonez.conf

* kubectl config set-context gitlabci@kubernetes  \
  --cluster=kubernetes \
  --user=gitlabci \
  --kubeconfig=gitlabci.conf

* kubectl config set-context jquinonez@kubernetes  \
  --cluster=kubernetes \
  --user=jquinonez \
  --kubeconfig=jquinonez.conf

* kubectl config view --kubeconfig=gitlabci.conf
* kubectl config view --kubeconfig=jquinonez.conf


* kubectl create secret docker-registry docker-registry-creds --docker-server="https://harbornode.mp" --docker-email=k8s-dev@mp.gob.gt --docker-username=k8s-development --docker-password=4dF22s9a23