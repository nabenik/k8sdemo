package me.tuxtor.mp.quickstart;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@ApplicationScoped
@Path("/hello")
public class Hello {

    @GET
    public String doHello() throws UnknownHostException {
        var ip = InetAddress.getLocalHost().toString();
        return "Esta es la version 1.3.0 en desarrollo " + ip;
    }
    
}
